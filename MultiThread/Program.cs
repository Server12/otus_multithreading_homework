﻿// See https://aka.ms/new-console-template for more information

using System.Diagnostics;

const int MaxIterations_1 = 100_000;
const int MaxIterations_2 = 1_000_000;
const int MaxIterations_3 = 10_000_000;

int[] array = Enumerable.Range(1, 100).ToArray();


Console.WriteLine($"Total sum threaded: {CalcSumThreaded()}");
Console.WriteLine($"Total sum Parallel Linq: {CalcSumParralelLinq( 2)}");
Console.WriteLine($"Total sum: {CalcSum()}");

MeasureCalcSumParallel(MaxIterations_1);
MeasureCalcSumParallel(MaxIterations_2);
MeasureCalcSumParallel(MaxIterations_3);

MeasureCalcSum(MaxIterations_1);
MeasureCalcSum(MaxIterations_2);
MeasureCalcSum(MaxIterations_3);

MeasureCalcSumThreaded(MaxIterations_1);
MeasureCalcSumThreaded(MaxIterations_2);
MeasureCalcSumThreaded(MaxIterations_3);


void MeasureCalcSum(int count)
{
    var stopWatch = Stopwatch.StartNew();
    for (int i = 0; i < count; i++)
    {
        CalcSum();
    }

    Console.WriteLine(
        $"CalcSum total time msec: {stopWatch.ElapsedMilliseconds} for count: {count}");
}

void MeasureCalcSumThreaded(int count)
{
    var thread = new Thread(() =>
    {
        var stopWatch = Stopwatch.StartNew();
        for (int i = 0; i < count; i++)
        {
            CalcSum();
        }

        Console.WriteLine(
            $"CalcSum Threaded total time msec: {stopWatch.ElapsedMilliseconds} for count: {count} ThreadId: {Environment.CurrentManagedThreadId}");
    })
    {
        Name = $"CalcSum"
    };
    thread.Start();
}

void MeasureCalcSumParallel(int count)
{
    var stopWatch = Stopwatch.StartNew();

    for (int i = 0; i < count; i++)
    {
        CalcSumParralelLinq(16);
    }

    Console.WriteLine(
        $"CalcSum Parallel Linq total time msec: {stopWatch.ElapsedMilliseconds} for count: {count}");
}

int CalcSum()
{
    var sum = 0;

    for (int j = 0; j < array.Length; j++)
    {
        sum += array[j];
    }

    return sum;
}

int CalcSumParralelLinq(int degree)
{
    return  array.AsParallel().WithDegreeOfParallelism(degree).Sum();
}

int CalcSumThreaded()
{
    int sum = 0;

    var thread = new Thread(() =>
    {
        Console.WriteLine($"CalcSum Threaded threadId: {Environment.CurrentManagedThreadId}");
        sum = CalcSum();
        Console.WriteLine($"CalcSum Threaded threadId: {Environment.CurrentManagedThreadId}");
    });
    thread.Start();
    thread.Join();

    return sum;
}